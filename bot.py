from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import logging
from openpyxl import load_workbook
import time

driver = webdriver.Firefox()
wait = WebDriverWait(driver, 5)
logging.basicConfig(filename='padron.log', filemode='a')
FILE_NAME = 'padron.xlsx'
workbook = load_workbook(FILE_NAME)
SAVE_AT_RECORD = 50
PARSERS = dict()

def fetch_users():
	ws = workbook.active
	return ws.iter_rows(min_row=12497, max_col=6, max_row=12500)

def login(wait, dni):
	elem = wait.until(EC.element_to_be_clickable((By.NAME, 'usuario')))
	elem.clear()
	elem.send_keys(dni)
	elem = wait.until(EC.element_to_be_clickable((By.NAME, 'password')))
	elem.send_keys(dni)
	elem.send_keys(Keys.RETURN)
	wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'notificaciones')))

def goto_reports(driver, wait):
	driver.get("https://guarani.unq.edu.ar/grado/historia_academica")


def extract_max_year(wait):
	ultimo_año_cursada = 0
	fecha_cursada = None 
	try:
		select = Select(wait.until(EC.element_to_be_clickable((By.ID, 'js-select-carreras'))))
		i = 0
		for option in select.options:
			select.select_by_index(i)
			
			ultimo_año_cursada, fecha_cursada = extract_max_year_and_date(wait, ultimo_año_cursada, fecha_cursada)
			
			select = Select(wait.until(EC.element_to_be_clickable((By.ID, 'js-select-carreras'))))
			i += 1
	except Exception as e:
		ultimo_año_cursada, fecha_cursada = extract_max_year_and_date(wait, ultimo_año_cursada, fecha_cursada)

	return ultimo_año_cursada, fecha_cursada

def extract_max_year_and_date(wait, ultimo_año_cursada, fecha_cursada):
	try:
		ultimo_año_cursada_aux = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'titulo-corte'))).text
		fecha_cursada_aux = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'catedra_nombre'))).text
	except Exception as e:
		return ultimo_año_cursada, fecha_cursada

	try:
		ultimo_año_cursada_aux = int(ultimo_año_cursada_aux)
		if ultimo_año_cursada<ultimo_año_cursada_aux:
			ultimo_año_cursada = ultimo_año_cursada_aux
			fecha_cursada = fecha_cursada_aux
	except Exception as e:
		ultimo_año_cursada = 0
		raise e

	return ultimo_año_cursada, fecha_cursada

def log_detail(user_row, log):
	if not log["status"] in PARSERS:
		PARSERS[log["status"]] = ExcelParser.get_parser(log["status"])

	PARSERS[log["status"]].write(user_row, log)

def read_from_excel(driver):
	record_number = 1
	for user_row in fetch_users():
		user = str(user_row[0].value)
		if user == 'None':
			break
		try:
			login(wait, user)
			goto_reports(driver, wait)
			max_year, detalle = extract_max_year(wait)

			log_detail(user_row, {
				"dni": user,
				"status": "success",
				"year": max_year,
				"detail": detalle
				})

			driver.get("https://guarani.unq.edu.ar/grado/acceso/logout")
		except TimeoutException as e:
			log_detail(user_row, {
			"dni": user,
			"status": "error",
			"message": "no se pudo iniciar sesion"
			})

			driver.get("https://guarani.unq.edu.ar")
			logging.exception(e)

		if record_number == SAVE_AT_RECORD:
			workbook.save(filename=FILE_NAME)
			record_number = 0
		record_number += 1


class ExcelParser:
	def write(self, row, data):
		row[3].value = data["status"]
		self.write_extra(row, data)

	@classmethod
	def get_parser(cls, status):
		return list(filter(lambda sb: sb().can_handle(status), cls.__subclasses__()))[0]()

class SuccessExcelParser(ExcelParser):
	def write_extra(self, row, data):
		row[4].value = data["year"]
		row[5].value = data["detail"]

	def can_handle(self, status):
		return status == "success"


class ErrorExcelParser(ExcelParser):
	def write_extra(self, row, data):
		row[4].value = data["message"]

	def can_handle(self, status):
		return status == "error"

if __name__ == '__main__':
	start_time = time.time()
	driver.get("https://guarani.unq.edu.ar")

	read_from_excel(driver)

	workbook.save(filename=FILE_NAME)
	driver.close()
	elapsed_time = time.time() - start_time

	print(elapsed_time)